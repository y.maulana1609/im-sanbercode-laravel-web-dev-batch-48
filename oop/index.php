<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <?php
  require_once('animal.php');
  require('Frog.php');
  require('Ape.php');

  ;

  // Release 0
  $sheep = new Animal('shaun');

  echo $sheep->name; // "shaun"
  echo $sheep->legs; // 4
  echo $sheep->cold_blooded; // "no"
  echo "<br>";

  echo $sheep->get_name();
  echo $sheep->get_legs();
  echo $sheep->get_cold_blooded();

  // Release 1
  
  $sungokong = new Ape("kera sakti");
  echo $sungokong->get_name();
  echo $sungokong->get_legs();
  echo $sungokong->get_cold_blooded();
  $sungokong->yell(); // "Auooo"
  
  $kodok = new Frog("buduk");
  echo $kodok->get_name();
  echo $kodok->get_legs();
  echo $kodok->get_cold_blooded();
  $kodok->jump(); // "hop hop"
  
  ?>
</body>

</html>