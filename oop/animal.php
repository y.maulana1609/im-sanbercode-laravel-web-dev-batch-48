<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>

  <?php
  // Release 0
  class Animal
  {
    public $name;
    public $legs = 4;
    public $cold_blooded = 'no';
    public function __construct($name)
    {
      $this->name = $name;
      echo "<br>";
    }

    function get_name()
    {
      echo "<br>";
      echo "Name: ";
      echo $this->name;

    }
    function get_legs()
    {
      echo "<br>";
      echo "legs: ";
      echo $this->legs;
    }
    function get_cold_blooded()
    {
      echo "<br>";
      echo "cold blooded: ";
      echo $this->cold_blooded;
    }
  }

  ?>
</body>

</html>