<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <?php
  require_once('animal.php');
  // Release 1
  class Frog extends Animal
  {
    function jump()
    {
      echo "<br>";
      echo "Jump: ";
      echo 'Hop Hop';
    }
  }
  ?>
</body>

</html>