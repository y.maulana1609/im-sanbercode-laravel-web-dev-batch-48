<?php

use App\Http\Controllers\GameController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route untuk halaman game form input.
Route::get('/game/create', [GameController::class, 'create']);
// Route untuk menyimpan inputan game form input.
Route::post('/game', [GameController::class, 'store']);

// Route untuk melihat semua data dari game table
Route::get('/game', [GameController::class, 'index']);
// Route untuk melihat detail data dari game table berdasarkan id
Route::get('/game/{id}', [GameController::class, 'show']);

// Route untuk mengarah ke form udpate data berdasarkan id
Route::get('/game/{id}/edit', [GameController::class, 'edit']);
// Route untuk meng-update data dari game table berdasarkan id
Route::put('/game/{id}', [GameController::class, 'update']);

// Delete Data
// Route delete data berdasarkan id
Route::delete('/game/{id}', [GameController::class, 'destroy']);