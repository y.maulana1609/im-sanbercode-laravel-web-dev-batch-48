<!doctype html>

<html lang="en">

<head>

  <!-- Required meta tags -->

  <meta charset="utf-8">

  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"
    integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">

  <title>Create Data</title>

</head>

<body>

  <h2>Create Data Game</h2>

  <form method="post" action="/game" class="w-50 m-auto">
    @csrf
    <div class="form-group">
      <label for="inputNamaGame">Name:</label>
      <input class="form-control @error('name') is-invalid @enderror" type="text" name='name' id="inputNamaGame">
    </div>

    @error('name')
    <div class="alert alert-danger"><span>{{$message}}</span></div>
    @enderror

    <div class="form-outline">
      <label class="form-label" for="inputGamePlay">Game Play:</label>
      <textarea class="form-control" name="game_play" id="inputGamePlay" rows="4"></textarea>
    </div>

    <div class="form-group">
      <label for="inputDeveloper">Developer:</label>
      <input class="form-control" type="text" name='developer' id="inputDeveloper">
    </div>

    <div class="form-group">
      <label for="inputYear">Year:</label>
      <input class="form-control @error('year') is-invalid @enderror" type="number" name='year' id="inputYear">
    </div>

    @error('year')
    <div class="alert alert-danger"><span>{{$message}}</span></div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>



  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js"
    integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
    crossorigin="anonymous"></script>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF"
    crossorigin="anonymous"></script>

</body>

</html>