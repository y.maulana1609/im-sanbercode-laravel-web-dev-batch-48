<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{
    public function create()
    {
        return view('game.create');
    }

    public function store(Request $request)
    {
        // validation
        $request->validate([
            'name'      => 'required|min:3',
            'game_play' => 'required',
            'developer' => 'required',
            'year'      => 'required|min:4',
        ]);

        // insert data into database
        DB::table('game')->insert([
            'name'      => $request->input('name'),
            'game_play' => $request->input('game_play'),
            'developer' => $request->input('developer'),
            'year'      => $request->input('year'),
        ]);

        // return to game page
        return redirect('/game');

    }

    public function index()
    {
        $game = DB::table('game')->get();
        return view('game.index', compact('game'));
    }

    public function show($id)
    {
        $gameDetail = DB::table('game')->find($id);

        return view('game.show', compact('gameDetail'));
    }

    public function edit($id)
    {
        $gameDetail = DB::table('game')->find($id);

        return view('game.edit', compact('gameDetail'));
    }
    public function update($id, Request $request)
    {
        // validation
        $request->validate([
            'name'      => 'required|min:3',
            'game_play' => 'required',
            'developer' => 'required',
            'year'      => 'required|min:4',
        ]);

        // update data
        DB::table('game')
            ->where('id', $id)
            ->update(
                [
                    'name'      => $request->input('name'),
                    'game_play' => $request->input('game_play'),
                    'developer' => $request->input('developer'),
                    'year'      => $request->input('year'),
                ]
            );

        // return to game page
        return redirect('/game');
    }

    public function destroy($id)
    {
        DB::table('game')->where('id', '=', $id)->delete();

        return redirect('/game');
    }
}