<?php

use App\Http\Controllers\CastController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layout.master');
});

Route::get('/', function () {
    return view('pages.home');
});

Route::get('/table', function () {
    return view('pages.table');
});

Route::get('/data-tables', function () {
    return view('pages.data-tables');
});

// CRUD cast 
// Route untuk halaman cast form input 
Route::get('/cast/create', [CastController::class, 'create']);
// Route untuk menyimpan inputan cast form input ke database
Route::post('/cast', [CastController::class, 'store']);

// Route untuk melihat semua data dari cast table
Route::get('/cast', [CastController::class, 'index']);
// Route untuk melihat detail data dari cast table berdasarkan id
Route::get('/cast/{id}', [CastController::class, 'show']);

// Route untuk mengarah ke form udpate data berdasarkan id
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
// Route untuk meng-update data dari cast table berdasarkan id
Route::put('/cast/{id}', [CastController::class, 'update']);

// Delete Data
// Route delete data berdasarkan id
Route::delete('/cast/{id}', [CastController::class, 'destroy']);