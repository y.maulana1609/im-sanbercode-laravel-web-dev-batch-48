<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('pages.cast.create');
    }

    public function store(Request $request)
    {
        // validation
        $request->validate([
            'nama' => 'required|min:3',
            'umur' => 'required|max:2',
            'bio'  => 'required',
        ]);

        // insert data into database
        DB::table('cast')->insert([
            'nama' => $request->input('nama'),
            'umur' => $request->input('umur'),
            'bio'  => $request->input('bio'),
        ]);

        // return to cast page
        return redirect('/cast');

    }

    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('pages.cast.index', compact('cast'));
    }

    public function show($id)
    {
        $castDetail = DB::table('cast')->find($id);

        return view('pages.cast.detail', compact('castDetail'));
    }

    public function edit($id)
    {
        $castDetail = DB::table('cast')->find($id);

        return view('pages.cast.edit', compact('castDetail'));
    }
    public function update($id, Request $request)
    {
        // validation
        $request->validate([
            'nama' => 'required|min:3',
            'umur' => 'required|max:2',
            'bio'  => 'required',
        ]);

        // update data
        DB::table('cast')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request->input('nama'),
                    'umur' => $request->input('umur'),
                    'bio'  => $request->input('bio')
                ]
            );

        // return to cast page
        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', '=', $id)->delete();

        return redirect('/cast');
    }
}