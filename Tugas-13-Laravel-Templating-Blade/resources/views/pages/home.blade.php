@extends('layout.master')

@section('title')
  Welcome To Dashboard
@endsection

@section('content')

<div>
  <h5>
    <a href="/table">
      Go to Table pages
    </a>
  </h5>
  <br>
  <h5>
    <a href="/data-tables">
      Go to Data Tables pages
    </a>
  </h5>
</div>
@endsection