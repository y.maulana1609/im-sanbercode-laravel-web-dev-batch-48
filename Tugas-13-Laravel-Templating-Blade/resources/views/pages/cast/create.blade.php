@extends('layout.master')

@section('title')
  Cast Form Input
@endsection

@section('content')
  <div>
    <form method="post" action="/cast">
      @csrf
      <div class="form-group">
        <label for="inputNama">Nama Aktor/Aktris:</label>
        <input class="form-control @error('nama') is-invalid @enderror" type="text" name='nama' id="inputNama">
      </div>
      
      @error('nama')
        <div class="alert alert-danger"><span>input nama setidaknya ada 3 karakter</span></div>
      @enderror
      
      <div class="form-group">
        <label for="inputUmur">Umur:</label>
        <input class="form-control @error('umur') is-invalid @enderror" type="text" name="umur" id="inputUmur">
      </div>

      @error('umur')
        <div class="alert alert-danger"><span>input umur setidaknya tidak lebih dari 2 karakter</span></div>
      @enderror

      <div class="form-outline">
        <label class="form-label" for="inputBio">Bio:</label>
        <textarea class="form-control" name="bio" id="inputBio" rows="4"></textarea>
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </div>
@endsection