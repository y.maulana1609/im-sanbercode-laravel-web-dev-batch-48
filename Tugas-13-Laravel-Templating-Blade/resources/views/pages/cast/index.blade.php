@extends('layout.master')

@section('title')
  Cast 
@endsection

@section('content')
<a href="/cast/create" class="btn btn-sm btn-primary my-3">Add Cast</a>
<table class="table">
  <thead> 
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama Aktor/Aktris</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
    </tr>
  </thead>
  <tbody>
    @forelse ($cast as $key=>$value)
      <tr>
        <th scope="row">{{$key+1}}</th>
        <td>{{$value->nama}}</td>
        <td>{{$value->umur}}</td>
        <td> 
          <form action="/cast/{{$value->id}}" method='post'>
            @csrf
            @method('delete')
            <a href="/cast/{{$value->id}}" class="btn btn-info btm-sm">Detail</a>
            <a href="/cast/{{$value->id}}/edit" class="btn btn-warning btm-sm">Edit</a>
            <input type="submit" class="btn btn-danger btn-sm py-2" value="Delete">
          </form>
        </td>
      </tr>
    @empty
      <tr colspan="3">
        <td>No data</td>
      </tr> 
    @endforelse
    
  </tbody>
</table>
@endsection
