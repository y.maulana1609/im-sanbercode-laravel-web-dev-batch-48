@extends('layout.master')

@section('title')
  Cast Detail Info
@endsection

@section('content')

<h1 class="text-primary">{{$castDetail->nama}}</h1>
<p>Umur: {{$castDetail->umur}} Tahun</p>
<p>Bio. <br> {{$castDetail->bio}}</p>

<a href="/cast">back to cast data</a>


@endsection