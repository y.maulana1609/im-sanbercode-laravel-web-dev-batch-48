@extends('layout.master')

@section('title')
  Cast Form Input
@endsection

@section('content')

  <form method="Post" action="/cast/{{$castDetail->id}}">
    @csrf
    @method('put')
    
    <div class="form-group">
      <label for="inputNama">Nama Aktor/Aktris:</label>
      <input class="form-control type="text" name='nama' value="{{$castDetail->nama}}" id="inputNama" @error('nama') is-invalid @enderror">
    </div>
    
    @error('nama')
      <div class="alert alert-danger"><span>input nama setidaknya ada 3 karakter</span></div>
    @enderror
    
    <div class="form-group">
      <label for="inputUmur">Umur:</label>
      <input class="form-control type="text" name="umur" value="{{$castDetail->umur}}" id="inputUmur" @error('umur') is-invalid @enderror" >
    </div>

    @error('umur')
      <div class="alert alert-danger"><span>input umur setidaknya tidak lebih dari 2 karakter</span></div>
    @enderror

    <div class="form-outline">
      <label class="form-label" for="inputBio">Bio:</label>
      <textarea class="form-control" name="bio" id="inputBio" rows="4">{{$castDetail->bio}}</textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection