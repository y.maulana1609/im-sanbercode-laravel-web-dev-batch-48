<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('/pages/register');
    }
    public function welcome(Request $request)
    {
        $first_name  = $request->input('first_name');
        $last_name   = $request->input('last_name');
        $gender      = $request->input('gender');
        $nationality = $request->input('nationality');
        $languages   = $request->input('languages');
        $bio         = $request->input('bio');

        return view('/pages/welcome', ['first_name' => $first_name, 'last_name' => $last_name, 'gender' => $gender, 'nationality' => $nationality, 'languages' => $languages, 'bio' => $bio]);

    }
}