<!DOCTYPE html>
<html lang="en">

<head>
  <title>Halaman Pendaftaran</title>
</head>

<body>
  <h1>Buat Account Baru!</h1>
  <h3>Sign Up Form</h3>
  <form action='/welcome' method='post'>
    @csrf 
    <label>First name:</label>
    <br>
    <input type="text" name='first_name'>
    <br><br>

    <label>Last name:</label>
    <br>
    <input type="text" name='last_name'>
    <br><br>

    <label>Gender:</label>
    <br>
    <input type="radio" name='gender' value='1'>Male</input>
    <br>
    <input type="radio" name='gender' value='2'>Female</input>
    <br><br>

    <label>Nationality:</label><br>
    <select name='nationality'>
      <option value="Indonesia">Indonesia</option>
      <option value="Malaysia">Malaysia</option>
      <option value="Singapore">Singapore</option>
      <option value="Thailand">Thailand</option>
    </select>
    <br>
    <br>
    <label>Language Spoken:</label><br>
    <input type="checkbox" name='languages[]' value='bahasa'>Bahasa Indonesia</input>
    <br>
    <input type="checkbox" name='languages[]' value='english'>English</input>
    <br>
    <input type="checkbox" name='languages[]' value='other'>Other</input>
    <br>
    <br>

    <label>Bio:</label>
    <br>
    <textarea name='bio' rows="10" cols="50"></textarea>
    <br>
    <input type="submit" value='Sign Up'>
  </form>
</body>

</html>