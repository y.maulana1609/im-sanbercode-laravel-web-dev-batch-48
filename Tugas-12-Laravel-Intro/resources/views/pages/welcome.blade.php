<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>

<body>
  <h1>SELAMAT DATANG
    @if($gender==='1')
      Tn. 
    @else
      Ny. 
    @endif
    {{$first_name}} {{$last_name}}
  </h1>
  <h2>Terima kasih telah bergabung di Senberbook. Sosial Media kita bersama!</h2>
  <div>
    <p>Berikut adalah data diri Anda:</p>
    <p>Nama Lengkap: {{$first_name}} {{$last_name}}</p>
    <p>Jenis Kelamin:  @if($gender==='1')
      Laki-laki
    @else
      Perempuan 
    @endif</p>
    <p>Kewarganegaraan: {{$nationality}}</p>
    <p>Bahasa yang dikuasai:  {{implode(', ', $languages)}}
  
  </p>
    <p>Bio: {{$bio}}</p>
  </div>
</body>

</html>