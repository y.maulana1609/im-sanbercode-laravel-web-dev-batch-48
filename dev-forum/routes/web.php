<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\QuestionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layout.master');
});

// Route untuk halaman forum list 
// Route::get('/question', [QuestionController::class, 'index']);
// Route untuk halaman detail question 
// Route::get('/question/{id}', [QuestionController::class, 'show']);
// // Route untuk halaman detail question 
// Route::get('/question/create', [QuestionController::class, 'create']);
// Route::post('/question', [QuestionController::class, 'store']);

// Route::resource('question', 'QuestionController')->only([
//     'show',
//     'index'
// ]);

Route::group(['middleware' => ['auth']], function () {
    Route::resource('/profile', ProfileController::class)->only([
        'index',
        'update'
    ]);
    Route::resource('/category', CategoryController::class);
    Route::resource('/question', QuestionController::class)->except([
        'show',
        'index'
    ]);

});

Route::resource('/question', QuestionController::class)->only([
    'show',
    'index'
]);

Auth::routes();

// // Route untuk page category
// Route::get('/category', [CategoryController::class, 'index']);

// // Route untuk page create category
// Route::get('/category/create', [CategoryController::class, 'create']);
// // Route untuk tambah category
// Route::post('/category', [CategoryController::class, 'store']);

// // Route untuk tambah category
// Route::get('/category/{id}', [CategoryController::class, 'show']);
// // Route untuk tambah category
// Route::get('/category/{id}/edit', [CategoryController::class, 'edit']);
// // Route untuk tambah category
// Route::post('/category', [CategoryController::class, 'update']);

// Route untuk halaman detail question 
// Route::get('/question/create', [QuestionController::class, 'create']);
// Route::post('/question', [QuestionController::class, 'store']);
