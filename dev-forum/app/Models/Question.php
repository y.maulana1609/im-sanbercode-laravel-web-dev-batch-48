<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';
    protected $fillable = ['title', 'question_text', 'question_image', 'users_id', 'categories_id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'users_id');
    }

    public function categories()
    {
        return $this->belongsTo(Category::class, 'categories_id');
    }
    // public function answers()
    // {
    //     return $this->hasMany(Answer::class);
    // }
}