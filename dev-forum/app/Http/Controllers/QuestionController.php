<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Question;
use App\Models\User;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class QuestionController extends Controller
{
    public function index()
    {
        $users     = User::all();
        $questions = Question::all();
        return view('pages.question.index', compact('questions', 'users'));
    }
    public function show($id)
    {
        $questions  = Question::find($id);
        $categories = Category::all();
        return view('pages.question.show', compact('questions', 'categories'));
    }

    public function create()
    {
        //
        $categories = Category::all();
        return view('pages.question.create', compact('categories'));
    }

    public function store(Request $request)
    {
        //
        $request->validate([
            'title'          => 'required',
            'question_text'  => 'required',
            'question_image' => 'required|mimes:png,jpeg,jpg|max:2048',
            'categories_id'  => 'required',
        ]);

        $id = Auth::id();

        $imageFileName = time() . '.' . $request->question_image->extension();
        $request->question_image->move(public_path('image'), $imageFileName);

        $question = new Question;

        $question->title          = $request->title;
        $question->question_text  = $request->question_text;
        $question->categories_id  = $request->categories_id;
        $question->users_id       = $id;
        $question->question_image = $imageFileName;

        toastr()->success('Success', 'question has been created!');

        $question->save();

        return redirect('/question')->with('msg', 'your data was added successfully');
    }

    public function edit($id)
    {
        //
        $question = Question::find($id);
        $category = Category::all();
        return view('pages.question.edit', compact('question', 'category'));
    }

    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'title'          => 'required',
            'question_text'  => 'required',
            'question_image' => 'required|mimes:png,jpeg,jpg|max:2048',
            'categories_id'  => 'required',
        ]);

        $question = Question::find($id);
        if ($request->has('image')) {
            $path = "image/";
            File::delete($path . $question->question_image);

            $imageFileName = time() . '.' . $request->question_image->extension();

            $request->question_image->move(public_path('image'), $imageFileName);

            $question->question_image = $imageFileName;

            $question->save();
        }

        $question->title         = $request->title;
        $question->question_text = $request->question_text;
        $question->categories_id = $request->categories_id;

        $question->save();

        toastr()->success('Jawaban kamu berhasil diedit.', 'Berhasil!');
        return redirect('/quest$question')->with('msg', 'data successfully updated!');
    }

    public function destroy($id)
    {
        //
        $question = Question::find($id);
        $path     = "image/";
        File::delete($path . $question->question_image);
        $question->delete();
        toastr()->success('Berhasil', 'question Berhasil di hapus');
        return redirect('/question')->with('msg', 'data successfully deleted!');
    }


}