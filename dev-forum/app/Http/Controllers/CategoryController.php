<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;


class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('pages.category.index', compact('categories'));
    }
    public function create()
    {
        return view('pages.category.create');
    }

    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'category_name' => 'required',
        ]);

        Category::create([
            'category_name' => $request->category_name,
        ]);
        toastr()->success('category successfully created!');
        return redirect('/category');
    }
    public function show($id)
    {
        //
        $categories = Category::find($id);
        return view('pages.category.show', compact('categories'));
    }
    public function edit($id)
    {
        //
        $categories = Category::find($id);
        return view('pages.category.edit', compact('categories'));
    }
    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'category_name' => 'required',
        ]);
        $categories                = Category::find($id);
        $categories->category_name = $request->category_name;
        $categories->update();

        toastr()->success('category successfully updated!');
        return redirect('/category');
    }

    public function destroy($id)
    {
        //
        $categories = Category::find($id);
        $categories->delete();
        toastr()->success('category has been deleted!');
        return redirect('/category');
    }
}