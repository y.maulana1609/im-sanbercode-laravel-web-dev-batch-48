<nav class="sidebar-nav scroll-sidebar" data-simplebar="">
  <ul id="sidebarnav">
    <li class="nav-small-cap">
      <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
      <span class="hide-menu">FORUM</span>
    </li>
    <li class="sidebar-item">
      <a class="sidebar-link" href="/question" aria-expanded="false">
        <span>
          <i class="ti ti-messages"></i>
        </span>
        <span class="hide-menu">Forum</span>
      </a>
    </li>

    @auth
    <li class="sidebar-item">
      <a class="sidebar-link" href="/category" aria-expanded="false">
        <span>
          <i class="ti ti-category"></i>
        </span>
        <span class="hide-menu">Category</span>
      </a>
    </li>
    <li class="sidebar-item">
      <a class="sidebar-link" href="/profile" aria-expanded="false">
        <span>
          <i class="ti ti-user"></i>
        </span>
        <span class="hide-menu">Profile</span>
      </a>
    </li>
    @endauth

    <li class="nav-small-cap">
      <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
      <span class="hide-menu">AUTH</span>
    </li>

    @auth
    <li class="sidebar-item">
      <a class="sidebar-link" href="{{route('logout')}}" aria-expanded="false" onclick="event.preventDefault();
      document.getElementById('logout-form').submit();">
        <span>
          <i class="ti ti-logout"></i>
        </span>
        <span class="hide-menu">Logout</span>
      </a>
    </li>

    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
      @csrf
    </form>
    @endauth

    @guest
    <li class="sidebar-item">
      <a class="sidebar-link" href="/login" aria-expanded="false">
        <span>
          <i class="ti ti-login"></i>
        </span>
        <span class="hide-menu">Login</span>
      </a>
    </li>
    <li class="sidebar-item">
      <a class="sidebar-link" href="./authentication-register.html" aria-expanded="false">
        <span>
          <i class="ti ti-user-plus"></i>
        </span>
        <span class="hide-menu">Register</span>
      </a>
    </li>
    @endguest

</nav>