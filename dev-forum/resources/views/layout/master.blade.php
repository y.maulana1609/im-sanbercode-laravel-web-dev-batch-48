<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Dev Forum.</title>

  <link rel="shortcut icon" type="image/png" href="{{asset('/template/src/assets/images/logos/favicon.png')}}" />
  <link rel="stylesheet" href="{{asset('/template/src/assets/css/styles.min.css')}}" />

  @stack('styles')
</head>

<body>
  <!--  Body Wrapper -->
  <div class="page-wrapper" id="main-wrapper" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
    data-sidebar-position="fixed" data-header-position="fixed">
    <!-- Sidebar Start -->
    <aside class="left-sidebar">
      <!-- Sidebar scroll-->
      <div>
        <div class="brand-logo d-flex align-items-center justify-content-between">
          <h1>devForum</h1>
          <div class="close-btn d-xl-none d-block sidebartoggler cursor-pointer" id="sidebarCollapse">
            <i class="ti ti-x fs-8"></i>
          </div>
        </div>
        <!-- Sidebar navigation-->
        @include('layout.partial.sidebar')
        <!-- End Sidebar navigation -->
      </div>
      <!-- End Sidebar scroll-->
    </aside>
    <!--  Sidebar End -->
    <!--  Main wrapper -->
    <div class="body-wrapper">
      <!--  Header Start -->
      @include('layout.partial.navbar')
      <!--  Header End -->

      <!--  Content -->
      @include('layout.partial.content')

    </div>
  </div>
  <div class="text-center mb-2">
    <p class="mb-0 fs-4">Developed by
      <a href="https://adminmart.com/" target="_blank" class="pe-1 text-primary text-decoration-underline">Yusuf
        M.</a>
      &
      <a href="https://themewagon.com">Arsyita Naya</a>
    </p>
  </div>
  <script src="{{asset('template/src/assets/libs/jquery/dist/jquery.min.js')}}"></script>
  <script src="{{asset('template/src/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
  <script src="{{asset('template/src/assets/js/sidebarmenu.js')}}"></script>
  <script src="{{asset('template/src/assets/js/app.min.js')}}"></script>
  <script src="{{asset('template/src/assets/libs/apexcharts/dist/apexcharts.min.js')}}"></script>
  <script src="{{asset('template/src/assets/libs/simplebar/dist/simplebar.js')}}"></script>
  <script src="{{asset('template/src/assets/js/dashboard.js')}}"></script>

  <script>
    window.onload = function() {
    let links = document.querySelectorAll('.sidebar-link');
    let currentPath = window.location.pathname; // Get the current page's path
    
    links.forEach(function(link) {
      if (link.getAttribute('href').include(currentPath)) {
        link.classList.add('active');
      } else {
        link.classList.remove('active');
      }
    });
};
  </script>
  @stack('scripts')
</body>

</html>