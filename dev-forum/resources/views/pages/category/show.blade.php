@extends('layout.master')

@section('title')
Forum List
@endsection

@push('styles')
<link rel="stylesheet" href="{{asset('/template/src/assets/css/forum-list.styles.css')}}">
@endpush

@section('content')
<!-- Forum List -->
<div class="card">
  <h3 class="fw-bolder">Category:</h3>
  {{-- @forelse ($categories->forum as $item)
  @empty
  <h1>there are no questions in this category</h1>
  @endforelse --}}
  <h3 class="fw-bolder p-3">{{$categories->category_name}}</h3>
  <div class="card mb-2">
    <div class="card-body p-3 p-lg-4">
      <div class="d-flex gap-4">
        <a href="/" data-toggle="collapse" data-target=".forum-content"><img
            src="https://bootdey.com/img/Content/avatar/avatar1.png" class="mr-3 rounded-circle" width="50"
            alt="User" /></a>
        <div class="flex-grow-1">
          <h6><a href="#" data-toggle="collapse" data-target=".forum-content" class="text-body">Realtime fetching
              data</a></h6>
          <p class="text-dark">
            lorem ipsum dolor sit amet lorem ipsum dolor sit amet lorem ipsum dolor sit amet
          </p>
          <p class="text-muted d-flex gap-3">
            <a href="/">drewdan</a>
            replied
            <span class="text-dark fw-bolder">13 minutes ago</span>
          </p>
        </div>
        <div class="text-muted text-center align-self-center">
          <span class="fs-5"><i class="ti ti-message-circle ml-2"></i> 3</span>
        </div>
      </div>
    </div>
  </div>
  <a href="/category" class="btn btn-light mt-2"> Back </a>

</div>
<!-- /Forum List -->

@endsection