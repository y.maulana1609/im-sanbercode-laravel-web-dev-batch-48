@extends('layout.master')

@section('title')
Add Category
@endsection

@section('content')
<div class="col-12 mt-5 grid-margin stretch-card">
  <div class="card">
    <div class='card-header' style=' background-color: rgba(0, 0, 0, 0.1);'>
      <p class="fw-bolder mb-0">Category</p>
    </div>
    <div class="card-body py-2">
      <form action="/category" method="POST">
        @method('post')
        @csrf
        <div class="form-group">
          <input type="text" class="form-control mt-1 mb-3" name="category_name" id="category_name"
            placeholder="Input Category">
          @error('category_name')
          <div class="alert alert-danger">
            {{ $message }}
          </div>
          @enderror
        </div>
        <button type="submit" class="btn btn-primary">Add Category</button>
        <a href="/category" class="btn btn-light mx-4"> Back </a>
      </form>
    </div>
  </div>
</div>
@endsection