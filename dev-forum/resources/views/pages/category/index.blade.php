@extends('layout.master')

@section('title')
Category Page
@endsection

@push('scripts')
<script>
  $(document).ready(function() {
        $('#categoryTable').dataTable( {
          "order": [[ 0, 'asc' ], [ 1, 'asc' ]]
        } );
      });
</script>
<script src="https://cdn.datatables.net/v/bs5/dt-1.13.6/datatables.min.js"></script>

@endpush

@push('styles')
<link href="https://cdn.datatables.net/v/bs5/dt-1.13.6/datatables.min.css" rel="stylesheet">
@endpush

@section('content')
<div class="card">
  <div class="card-header">
    <a href="/category/create" class="btn btn-primary">Add Category</a>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <table id='categoryTable' class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>Kategori</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @forelse ($categories as $key=>$value)
        <tr>
          <td>{{$key + 1}}</td>
          <td>{{$value->category_name}}</td>
          <td>
            <form action="/category/{{$value->id}}" method="POST">
              <a href="/category/{{$value->id}}" class="btn btn-info py-2 btn-sm">Show</a>
              <a href="/category/{{$value->id}}/edit" class="btn btn-primary py-2 btn-sm">Edit</a>
              @csrf
              @method('DELETE')
              <input type="submit" class="btn btn-danger py-2 btn-sm" value="Delete">
            </form>
          </td>
        </tr>
        @empty
        <tr colspan="3">
          <td>No data</td>
        </tr>
        @endforelse
      </tbody>
    </table>
  </div>
</div>
@endsection