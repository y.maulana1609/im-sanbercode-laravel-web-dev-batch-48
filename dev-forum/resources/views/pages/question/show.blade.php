@extends('layout.master')
{{-- @push('scripts')
<script src="https://cdn.tiny.cloud/1/y0aei2hae802rnizhu7dvrnheypn7rvwfw3d2oao66re3c6t/tinymce/6/tinymce.min.js"
  referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: 'textarea',
    plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
    toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
    toolbar_mode: 'floating',
    tinycomments_mode: 'embedded',
    tinycomments_author: 'Author name',
  });
</script>
@endpush --}}

@section('content')
<div class="col-lg-12 stretch-card">
  <div class="card">
    <div class="card-body">
      @auth
      @if (Auth::user()->id == $questions->user->id)
      <i class="bi bi-three-dots-vertical float-right" id="dropdownMenuIconButton7" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false"></i>
      <div class="dropdown-menu" aria-labelledby="dropdownMenuIconButton7">
        <form action="/questions/{{$questions->id}}" method="POST">
          <a href="/questions/{{$questions->id}}/edit" class="dropdown-item">Edit</a>
          @csrf
          @method('DELETE')
          <input type="submit" class="dropdown-item" value="Delete">
        </form>
      </div>
      @endif
      @endauth
      <h4 class="card-title">{{$questions->title}}</h4>
      <h6 class="card-text">Ditulis oleh : {{$questions->user->name}}</h6>
      <p class="card-text"><small class="text-muted">{{$questions->created_at->diffForHumans()}}</small></p>
      <p class="card-description">{{$questions->question_text}}</p>
      <img src="{{asset('image/'. $questions->question_image)}}" style="width: 100vh; height: 400px" alt="">
      <p class="text-left">Category : {{$questions->categories->category_name}}</p>

      <a href="/question" class="btn btn-secondary btn-sm">Back</a>
    </div>
  </div>
</div>

@endsection