@extends('layout.master')

@section('title')
Add Question
@endsection

{{-- @push('scripts')
<script src="https://cdn.tiny.cloud/1/0b86ed3bw65caioyuc6c7m3ovs70djh3e4l0k5ia8h45ac8l/tinymce/6/tinymce.min.js"
  referrerpolicy="origin"></script>
@endpush --}}

@section('content')
<div class="col-12 grid-margin stretch-card">
  <div class="card">
    <div class="card-body">
      <h4 class="card-title">Ask a public question</h4>
      <p class="card-description">
        Feel free to ask a programming-related question or anything you want to ask.
      </p>
      <form action="/question" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
          <label for="title">Title</label>
          <input type="text" class="form-control" name="title" id="title" placeholder="Input title">
          @error('title')
          <div class="alert alert-danger">
            {{ $message }}
          </div>
          @enderror
        </div>
        <div class="form-group">
          <label for="title">Content</label>
          <textarea type="text" class="form-control" name="question_text" id="question_text"
            placeholder="Input your question"></textarea>
          @error('question_text')
          <div class="alert alert-danger">
            {{ $message }}
          </div>
          @enderror
        </div>
        <div class="form-group">
          <label for="title">Image</label>
          <input type="file" class="form-control" name="question_image" id="question_image"
            placeholder="Choose your image file">
        </div>
        @error('question_image')
        <div class="alert alert-danger">
          {{ $message }}
        </div>
        @enderror
        <div class="form-group">
          <label for="title">Category</label>
          <select class="form-control" name="categories_id" id="" placeholder="Input Category">
            <option value="">--Choose the Category--</option>
            @forelse ($categories as $item)
            <option value="{{$item->id}}">{{$item->category_name}}</option>
            @empty
            <option value="">Category not found!</option>
            @endforelse
            @error('categories_id')
            <div class="alert alert-danger">
              {{ $message }}
            </div>
            @enderror
          </select>
        </div>
        <button type="submit" class="btn btn-primary">Post</button>
        <a href="/question" class="btn btn-light"> Back </a>
      </form>
    </div>
  </div>
</div>
@endsection

{{-- @push('scripts')
<script>
  tinymce.init({
      selector: 'textarea',
      plugins: 'a11ychecker advcode casechange export formatpainter image editimage linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tableofcontents tinycomments tinymcespellchecker',
      toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter image editimage pageembed permanentpen table tableofcontents',
      toolbar_mode: 'floating',
      tinycomments_mode: 'embedded',
      tinycomments_author: 'Author name',
    });
</script>
@endpush --}}