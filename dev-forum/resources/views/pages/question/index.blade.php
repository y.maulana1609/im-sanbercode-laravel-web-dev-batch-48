@extends('layout.master')

@section('title')
Forum List
@endsection
@push('styles')
<link rel="stylesheet" href="{{asset('/template/src/assets/css/forum-list.styles.css')}}">
@endpush
@section('content')
@if (session('msg'))
<div class="alert alert-success">
  <p> {{session('msg')}} </p>
</div>
@endif
<!-- Forum List -->
<div class="container mt-2">
  <div class="card-header mb-4">
    <a href="/question/create" class="btn btn-primary">Add Question</a>
  </div>
  @forelse ($questions as $item)
  <div class="card mb-2">
    <div class="card-body p-3 p-lg-4">
      <div class="d-flex gap-4">
        <a href="/question" data-toggle="collapse" data-target=".forum-content"><img
            src="https://bootdey.com/img/Content/avatar/avatar1.png" class="mr-3 rounded-circle" width="50"
            alt="User" /></a>
        <div class="flex-grow-1">
          <h6>
            <a href="/question/{{$item->id}}" data-toggle="collapse" data-target=".forum-content"
              class="text-body fw-bolder">
              {{$item->title}}
            </a>
          </h6>
          <p class="text-dark">
            {{$item->question_text}}
          </p>
          <p class="text-muted d-flex gap-3 mb-0">
            <a href="/">{{$item->user->name}}</a>
            replied
            <span class="text-dark fw-bolder">{{$item->created_at->diffForHumans()}}</span>
          </p>
          <h6 class="mb-0">category: <a href="/category/{{$item->categories->categories_id}}" class="text-bold">
              {{$item->categories->category_name}}
          </h6>
        </div>
        <div class="text-muted text-center align-self-center">
          <span class="fs-5"><i class="ti ti-message-circle ml-2"></i> 3</span>
        </div>
      </div>
    </div>
  </div>
  @empty

  @endforelse


</div>
<!-- /Forum List -->

@endsection