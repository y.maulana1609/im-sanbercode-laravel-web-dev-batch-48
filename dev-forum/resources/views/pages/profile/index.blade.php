@extends('layout.master')

@section('title')
Add Category
@endsection

@section('content')
<form class="mt-4" action="/profile/{{$profile->id}}" method="POST">
  @csrf
  @method('put')

  <div class="form-group">
    <label>Nama</label>
    <input type="text" class="form-control" disabled value="Name">
  </div>

  <div class="form-group">
    <label>Email</label>
    <input type="text" class="form-control" disabled value="email@email.com">
  </div>

  <div class="form-group">
    <label>Umur</label>
    <div class="input-group">
      <input type="number" class="form-control" name="umur" value="18">
      <div class="input-group-prepend">
        <span class="input-group-text">Tahun</span>
      </div>
    </div>
  </div>
  @error('umur')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Alamat</label>
    <textarea name="alamat" class="form-control" rows="3">jl.pinang raya</textarea>
  </div>
  @error('alamat')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <div class="form-group">
    <label>Biodata</label>
    <textarea name="biodata" class="form-control" rows="6">ini bio gueh</textarea>
  </div>
  @error('bio')
  <div class="alert alert-danger">{{ $message }}</div>
  @enderror

  <button type="submit" class="btn btn-primary mr-2">Simpan</button>
  <a href="/pertanyaan" class="btn btn-light"> Kembali </a>
</form>
@endsection